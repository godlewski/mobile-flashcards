#!/usr/bin/node
/*jslint nomen: true, newcap: true, continue: true */
/*global require, CSSScanner, __dirname, process, jscsspToken, CountLF */

var fs = require('fs'),
    vm = require('vm'),
    util = require('util'),
    includeInThisContext,
    paramNo,
    argvlen = process.argv.length,
    css,
    scanner,
    cur,
    next,
    tokens,
    definitionOpen = false,
    atRuleOpen = false,
    t,
    errors,
    i,
    len,
    debug = false;


function getName(type) {
    'use strict';
    var name;
    for (name in jscsspToken) {
        if (jscsspToken[name] === type) {
            return name;
        }
    }
    return null;
}

function showError(line, msg) {
    'use strict';
    errors += 1;
    console.log('Line ' + line + ': ' + msg);
}

includeInThisContext = function include(path) {
    'use strict';
    var code = fs.readFileSync(path);
    vm.runInThisContext(code, path);
};

includeInThisContext = includeInThisContext.bind(this);

includeInThisContext(__dirname + '/cssParser.js');

t = jscsspToken;

if (process.argv[2] === undefined) {
    console.log('0 CSS files checked, no errors found.');
    process.exit(0);
}
if (process.argv[2] === '--debug') {
    debug = true;
    paramNo = 3;
} else {
    paramNo = 2;
}

for (paramNo; paramNo < argvlen; paramNo += 1) {

    console.log('----- FILE  :  ' + process.argv[paramNo] + ' -----');

    css = fs.readFileSync(process.argv[paramNo]);
    scanner = new CSSScanner(css);
    tokens = [];
    definitionOpen = false;
    errors = 0;

    while ((cur = scanner.nextToken()).isNotNull()) {
      /**
       * Token line number (might be imprecise in case of multiline whitespace)
       */
        cur.line = CountLF(scanner.getAlreadyScanned());
        tokens.push(cur);
    }
    // we need two more tokens
    tokens.push(cur);
    tokens.push(cur);

    // TODO: optimize for one-pass processing
    for (i = 0, len = tokens.length; i < len; i += 1) {
        cur = tokens[i];
        next = tokens[i + 1];

        switch (cur.type) {
        case t.ATRULE_TYPE:
            if (cur.value === '@font-face') {
                // @font-face behaves like a selector, not like an @rule
                definitionOpen = true;
            } else {
                // Remember if we're inside an @rule definition (till opening brace)
                atRuleOpen = true;
            }
            break;
        case t.SYMBOL_TYPE:
            switch (cur.value) {
            // Remember if we're inside a CSS definition
            // or close the atRule flag
            case '{':
                if (atRuleOpen) {
                    atRuleOpen = false;
                } else {
                    definitionOpen = true;
                }
                break;
            case '}':
                definitionOpen = false;
                break;
            case ':':
                if (!definitionOpen && !atRuleOpen) {
                    // Rule: Pseudoclasses come right after the basic selector
                    // td: nth-child(2) // function type
                    // td: last-of-type // ident type
                    if (next.type === t.WHITESPACE_TYPE) {
                        showError(cur.line, 'Unexpected whitespace after ":"');
                    } else if (next.type !== t.FUNCTION_TYPE &&
                            next.type !== t.IDENT_TYPE) {
                        showError(cur.line, 'Unexpected "' + next.value + '" after ":"');
                    }
                    break;
                }
                // Rule: Exactly one space before property value (after ':')
                // margin:2px;
                if (next.type !== t.WHITESPACE_TYPE) {
                    showError(cur.line, 'Missing space after ":"');
                // margin:   2px;
                } else if (next.value !== ' ') {
                    showError(cur.line,
                        'Unexpected whitespace before "' +
                         tokens[i + 2].value + '"');
                }
                break;

            }
            break;

        case t.WHITESPACE_TYPE:
            // Rule: No whitespace at end of line
            if (cur.value.match(/[ \t]\n/)) {
                showError(tokens[i - 1].line, 'Extra space at end of line');
            }
            // Rule: No tabs, only spaces
            if (cur.value.indexOf('\t') !== -1) {
                showError(cur.line,
                    'Illegal tab in whitespace before "' + next.value + '"');
                continue;
            }
            break;

        case t.IDENT_TYPE:
            if (definitionOpen) {
                // Rule: No whitespace after property name (before ':')
                // margin : 2px;
                if (next.type === t.WHITESPACE_TYPE &&
                        tokens[i + 2].type === t.SYMBOL_TYPE &&
                        tokens[i + 2].value === ':') {
                    showError(cur.line, 'Illegal whitespace before ":"');
                }
            // Rule: Exactly one space after selector
            // table{
            } else if (next.type === t.SYMBOL_TYPE && next.value === '{') {
                showError(cur.line, 'Missing space before "{"');

            // table    {
            } else if (next.type === t.WHITESPACE_TYPE &&
                    next.value !== ' ' &&
                    tokens[i + 2].type === t.SYMBOL_TYPE &&
                    tokens[i + 2].value === '{') {
                showError(cur.line, 'Unexpected whitespace before "{"');
            }
            break;
        }

        if (debug) {
            // print out info about each token
            console.log(
                cur,
                getName(cur.type),
                definitionOpen,
                CountLF(scanner.getAlreadyScanned())
            );
        }

    } // end for (tokens)
} // end for (files)

if (errors) {
    console.log('Found ' + errors + ' errors in ' +
        (argvlen - 2) + ' checked files.');
    process.exit(1);
} else {
    console.log((argvlen - 2 - (debug ? 1 : 0)) + "CSS files checked, no errors found.");
    process.exit(0);
}
