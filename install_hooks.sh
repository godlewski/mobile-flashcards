#!/bin/bash

# remove previous hooks
echo "Moving previous hooks out of the way, if any found..."
mv -i .git/hooks/applypatch-msg .git/hooks/applypatch-msg.old 2>/dev/null
mv -i .git/hooks/pre-applypatch .git/hooks/pre-applypatch.old 2>/dev/null
mv -i .git/hooks/post-applypatch .git/hooks/post-applypatch.old 2>/dev/null
mv -i .git/hooks/pre-commit .git/hooks/pre-commit.old 2>/dev/null
mv -i .git/hooks/prepare-commit-msg .git/hooks/prepare-commit-msg.old 2>/dev/null
mv -i .git/hooks/commit-msg .git/hooks/commit-msg.old 2>/dev/null
mv -i .git/hooks/post-commit .git/hooks/post-commit.old 2>/dev/null
mv -i .git/hooks/pre-rebase .git/hooks/pre-rebase.old 2>/dev/null
mv -i .git/hooks/post-checkout .git/hooks/post-checkout.old 2>/dev/null
mv -i .git/hooks/post-merge .git/hooks/post-merge.old 2>/dev/null
mv -i .git/hooks/pre-auto-gc .git/hooks/pre-auto-gc.old 2>/dev/null
mv -i .git/hooks/post-rewrite .git/hooks/post-rewrite.old 2>/dev/null

# add symlinks
echo "Installing new hooks..."
ln -s ../../hooks/applypatch-msg .git/hooks/applypatch-msg
ln -s ../../hooks/pre-applypatch .git/hooks/pre-applypatch
ln -s ../../hooks/post-applypatch .git/hooks/post-applypatch
ln -s ../../hooks/pre-commit .git/hooks/pre-commit
ln -s ../../hooks/prepare-commit-msg .git/hooks/prepare-commit-msg
ln -s ../../hooks/commit-msg .git/hooks/commit-msg
ln -s ../../hooks/post-commit .git/hooks/post-commit
ln -s ../../hooks/pre-rebase .git/hooks/pre-rebase
ln -s ../../hooks/post-checkout .git/hooks/post-checkout
ln -s ../../hooks/post-merge .git/hooks/post-merge
ln -s ../../hooks/pre-auto-gc .git/hooks/pre-auto-gc
ln -s ../../hooks/post-rewrite .git/hooks/post-rewrite

echo "Hooks installation done."
