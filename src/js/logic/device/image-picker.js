/*global define, tizen*/

// MOCK
define(['libs/q'], function imagePickerModule(q) {
    'use strict';

    function emptyPromise() {
        return q();
    }

    return {
        getImages: emptyPromise,
        imageAsBase64: emptyPromise
    };

});
