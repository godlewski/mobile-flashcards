/*global define, tizen, console, Image, window, document*/

define(['libs/q'], function imagePickerModule(Q) {
    'use strict';

    var filter = null;

    try {
        filter = new tizen.CompositeFilter('UNION', [
            new tizen.AttributeFilter('mimeType', 'EXACTLY', 'image/jpeg'),
            new tizen.AttributeFilter('mimeType', 'EXACTLY', 'image/png')
        ]);
    } catch (e) {
        console.error(e);
    }

    function comparePaths(file1, file2) {
        return file1.path.localeCompare(file2.path);
    }

    function getImages() {
        return Q.Promise(function promise(resolve, reject) {
            tizen.content.find(function onContentFound(contents) {
                    var result = [], content = null, i = 0;

                    for (i = 0; i < contents.length; i += 1) {
                        content = contents[i];
                        result.push({
                            title: content.title,
                            path: content.contentURI,
                            thumbnailPath: content.thumbnailURIs.length ?
                                content.thumbnailURIs[0] : null
                        });
                    }
                    resolve(result.sort(comparePaths));
                },
                reject,
                null,
                filter);
        });
    }

    function resizeImage(base64Image, extension) {
        return Q.Promise(function promise(resolve) {
            var sourceImage = new Image(),
                desiredWidth = parseInt(window.innerWidth * 0.9),
                desiredHeight = parseInt(window.innerHeight * 0.7);

            sourceImage.onload = function onLoad() {
                var canvas = document.createElement('canvas'),
                    width = desiredWidth,
                    height = desiredWidth * sourceImage.height / sourceImage.width;

                if (height > desiredHeight) {
                    height = desiredHeight;
                    width = desiredHeight * sourceImage.width / sourceImage.height;
                }
                canvas.width = width;
                canvas.height = height;

                canvas.getContext('2d').drawImage(sourceImage, 0, 0, width, height);

                resolve(canvas.toDataURL('image/' + extension));
            };

            sourceImage.src = base64Image;
        });
    }

    function imageAsBase64(path) {
        var extension = path.split('.').pop(),
            file = null;

        if (extension === 'jpg') {
            extension = 'jpeg';
        }
        return Q.Promise(function promise(resolve, reject) {
            tizen.filesystem.resolve(path, resolve, reject, 'r');
        }).then(function onOpened(receivedFile) {
            file = receivedFile;
            return Q.Promise(function promise(resolve, reject) {
                file.openStream('r', resolve, reject);
            });
        }).then(function onOpenedStream(fileStream) {
            var byte64 = fileStream.readBase64(file.fileSize);
            fileStream.close();
            return 'data:image/' + extension + ';base64,' + byte64;
        }).then(function onGotOriginalImage(base64Image) {
            return resizeImage(base64Image, extension);
        });
    }

    function rotate(base64Image) {
        var extension = base64Image.substring(11, base64Image.indexOf(';'));
        return Q.Promise(function promise(resolve) {
            var sourceImage = new Image();

            sourceImage.onload = function onLoad() {
                var canvas = document.createElement('canvas'),
                    context = canvas.getContext('2d'),
                    width = sourceImage.width,
                    height = sourceImage.height;

                canvas.width = height;
                canvas.height = width;
                context.translate(height / 2, width / 2);
                context.rotate(-Math.PI / 2);
                context.drawImage(sourceImage, -width / 2, -height / 2, width, height);

                resolve(resizeImage(canvas.toDataURL('image/' + extension), extension));
            };

            sourceImage.src = base64Image;
        });
    }

    return {
        getImages: getImages,
        imageAsBase64: imageAsBase64,
        rotate: rotate
    };

});
