/*global define, tizen, document, history, window*/

define(function tizenModule() {
    'use strict';

    if (!window.tizen) {
        return;
    }

    document.addEventListener('tizenhwkey', function onHardwareKey(event) {
        if (event.keyName === 'back') {
            if (document.getElementById('start-page').getAttribute('nav-view') === 'active') {
                tizen.application.getCurrentApplication().exit();
            } else {
                history.back();
            }
        }
    });
});
