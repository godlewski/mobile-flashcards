/*global define*/

define(function rotation() {
    'use strict';

    var angle;

    function isFront() {
        return angle <= 90 || angle > 270;
    }

    function restart() {
        angle = 0;
    }

    function start() {
        if (angle === undefined) {
            angle = 0;
        } else {
            angle = isFront() ? 0 : 180;
        }
    }

    function fix(angle) {
        while (angle < 0) {
            angle += 360;
        }
        return angle % 360;
    }

    function rotate(diff) {
        angle = fix(angle + diff);
        return angle;
    }

    function getAngle() {
        return angle;
    }

    return {
        restart: restart,
        start: start,
        rotate: rotate,
        isFront: isFront,
        getAngle: getAngle
    };
});
