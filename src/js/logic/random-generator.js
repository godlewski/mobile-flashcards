/*global define, Math*/

define(function randomGenerator() {
    'use strict';

    var TIME_WEIGHTS = [{
            time: 60000,    // less than 1 minute
            weight: 1
        }, {
            time: 300000,   // less than 5 minutes
            weight: 10
        }, {
            time: 3600000,  // less than 1 hour
            weight: 40
        }, {
            time: 86400000, // less than 1 day
            weight: 75
        }, {
            time: null,     // more than 1 day
            weight: 100
        }],

        STARS_WEIGHTS = [100, 80, 50, 30, 10, 1];

    function calculateWeight(flashcard, currentTime) {
        var timeWeight,
            starsWeight = STARS_WEIGHTS[flashcard.stars],
            time = currentTime - flashcard.lastModified,
            i, len, weightElement;

        for (i = 0, len = TIME_WEIGHTS.length; i < len; i += 1) {
            weightElement = TIME_WEIGHTS[i];
            if (!weightElement.time || time < weightElement.time) {
                timeWeight = weightElement.weight;
                break;
            }
        }

        return timeWeight + starsWeight;
    }

    function calculateWeights(flashcards) {
        var result = [], i, len, flashcard, weight,
            sum = 0, currentTime = new Date().getTime(),
            indicesMap = {};

        for (i = 0, len = flashcards.length; i < len; i += 1) {
            flashcard = flashcards[i];
            indicesMap[flashcard.id] = i;

            weight = calculateWeight(flashcard, currentTime);
            result.push({
                flashcard: flashcard,
                weight: weight
            });
            sum += weight;
        }

        return {
            weights: result,
            sum: sum,
            indicesMap: indicesMap,
            flashcards: flashcards
        };
    }

    function findFlashcard(value, weights) {
        var i, len, sum = 0, weight;

        for (i = 0, len = weights.length; i < len; i += 1) {
            weight = weights[i].weight;
            sum += weight;
            if (value < sum) {
                return weights[i].flashcard;
            }
        }
    }

    function next() {
        var data = this.privateData,
            value;

        if (data.weights.length === 0) {
            return null;
        }

        value = Math.random() * data.sum;
        return findFlashcard(value, data.weights);
    }

    function updateFlashcard(flashcard) {
        var flashcards = this.privateData.flashcards,
            map = this.privateData.indicesMap,
            weights = this.privateData.weights;

        if (map[flashcard.id] !== undefined) {
            flashcards[map[flashcard.id]] = flashcard;
            weights[map[flashcard.id]].flashcard = flashcard;
            var elementToUpdate = weights[map[flashcard.id]],
                oldWeight = elementToUpdate.weight,
                newWeight = calculateWeight(flashcard, new Date().getTime());

            elementToUpdate.weight = newWeight;
            this.privateData.sum += (newWeight - oldWeight);
        }

    }

    function removeFlashcard(flashcard) {
        var flashcards = this.privateData.flashcards,
            index = this.privateData.indicesMap[flashcard.id];
        if (index !== undefined) {
            flashcards.splice(index, 1);
            this.privateData = calculateWeights(flashcards);
        }
    }

    function addFlashcards(flashcard) {
        var flashcards = this.privateData.flashcards;
        flashcards.push(flashcard);
        this.privateData = calculateWeights(flashcards);
    }

    function RandomGenerator(flashcards) {
        this.privateData = calculateWeights(flashcards);
    }

    RandomGenerator.prototype = {
        next: next,
        updateFlashcard: updateFlashcard,
        removeFlashcard: removeFlashcard,
        addFlashcard: addFlashcards
    };

    return RandomGenerator;
});
