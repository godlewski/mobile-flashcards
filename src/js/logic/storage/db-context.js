/*global define, openDatabase*/

define(['libs/q'], function dbContextModule(Q) {
    'use strict';

    var DB_NAME = 'Flashcards',
        DB_VERSION = '1.0',
        DB_SIZE = 30 * 1024 * 1024,
        db = openDatabase(DB_NAME, DB_VERSION, DB_NAME, DB_SIZE);

    function transaction(fn) {
        return function promisedFunction() {
            var outArgs = arguments;

            return Q.Promise(function promisedFunction(resolve, reject) {
                db.transaction(function transactionContent(tx) {
                    var args = [], i = 0, len = outArgs.length;
                    for (; i < len; i += 1) {
                        args.push(outArgs[i]);
                    }
                    fn.apply(null, args.concat([tx, resolve, function onError(t, e) {
                        reject(e);
                    }]));
                }, reject);
            });
        };
    }

    return {
        transaction: transaction
    };
});
