/*global define*/

define(['logic/storage/sql', 'logic/storage/db-context'], function databaseModule(sql, db) {
    'use strict';

    function createTables(tx, sqls, onSuccess, onError) {
        if (sqls.length === 0) {
            onSuccess(tx);
            return;
        }

        var sqlStatement = sqls.shift();

        tx.executeSql(sqlStatement, [], function onComplete(tx) {
            createTables(tx, sqls, onSuccess, onError);
        }, onError);
    }

    function addSampleFlashcards(categoryId, flashcards, tx, onSuccess, onError) {
        if (flashcards.length === 0) {
            onSuccess(tx);
            return;
        }

        var flashcard = flashcards.shift(),
            weekMillis = 1000 * 60 * 60 * 24 * 7,
            date = new Date().getTime() - weekMillis;

        addFlashcard({
            categoryId: categoryId,
            front: {
                text: flashcard.front,
                image: flashcard.frontImage
            },
            back: {
                text: flashcard.back,
                image: flashcard.backImage
            },
            stars: 0,
            date: date
        }, tx, function onComplete(flashcardId, tx) {
            addSampleFlashcards(categoryId, flashcards, tx, onSuccess, onError);
        }, onError);
    }

    function insertSampleDb(dbItems, tx, onSuccess, onError) {
        if (dbItems.length === 0) {
            onSuccess(tx);
            return;
        }

        var dbItem = dbItems.shift();

        addCategory({
            name: dbItem.name,
            count: 0,
            active: dbItem.active
        }, tx, function onComplete(categoryId, tx) {
            addSampleFlashcards(categoryId, dbItem.flashcards, tx, function onComplete(tx) {
                insertSampleDb(dbItems, tx, onSuccess, onError);
            }, onError);
        }, onError);
    }

    function createSampleDb(tx, onSuccess, onError) {
        tx.executeSql(sql.select.initialized, [], function onComplete(tx, result) {
            if (result.rows.length === 0) {
                insertSampleDb(sql.sampleDB.concat([]), tx, function onInserted(tx) {
                    tx.executeSql(sql.insert.initialized, [], onSuccess, onError);
                }, onError);
            } else {
                onSuccess(tx);
            }
        });
    }

    function init(tx, onSuccess, onError) {
        createTables(tx, sql.createTables.concat([]), function onComplete(tx) {
            createSampleDb(tx, onSuccess, onError);
        }, onError);
    }

    function categoryToRecord(category) {
        var record = {
            active: category.active ? 1 : 0,
            count: category.count,
            name: category.name
        };
        if (category.id) {
            record.id = category.id;
        }
        return record;
    }

    function recordToCategory(record) {
        return {
            id: record.id,
            name: record.name,
            count: record.count,
            active: parseInt(record.active, 10) === 1
        };
    }

    function recordToFlashcard(record) {
        return {
            id: record.id,
            categoryId: record.categoryId,
            stars: record.stars,
            front: {
                text: record.frontText,
                image: record.frontImage
            },
            back: {
                text: record.backText,
                image: record.backImage
            },
            lastModified: record.lastModified
        };
    }

    function addCategory(category, tx, onSuccess, onError) {
        var record = categoryToRecord(category);

        tx.executeSql(sql.select.categoryByName, [category.name], function onSelectCategoryComplete(tx, result) {
            if (result.rows.length === 0) {
                tx.executeSql(sql.insert.category, [
                    record.name,
                    record.count,
                    record.active
                ], function onInsertComplete(tx, result) {
                    onSuccess(result.insertId, tx);
                }, onError);
            } else {
                onError(new Error('Category already exists'));
            }
        }, onError);
    }

    function getCategories(tx, onSuccess, onError) {
        tx.executeSql(sql.select.categories, [], function onSelectComplete(tx, result) {
            var i = 0, rows = result.rows, len = rows.length, record = null, categories = [];
            for (; i < len; i += 1) {
                record = rows.item(i);
                categories.push(recordToCategory(record));
            }
            onSuccess(categories);
        }, onError);
    }

    function removeCategory(id, tx, onSuccess, onError) {
        tx.executeSql(sql.remove.flashcards, [id], function onDeleteComplete(tx) {
            tx.executeSql(sql.remove.category, [id], onSuccess, onError);
        }, onError);
    }

    function setCategoryName(id, newName, tx, onSuccess, onError) {
        tx.executeSql(sql.update.categoryName, [newName, id], onSuccess, onError);
    }

    function setCategoryActive(id, active, tx, onSuccess, onError) {
        tx.executeSql(sql.update.categoryActive, [Number(active), id], onSuccess, onError);
    }

    function getCategoryByName(name, tx, onSuccess, onError) {
        tx.executeSql(sql.select.categoryByName, [name], function onSelectComplete(tx, result) {
            if (result.rows.length === 0) {
                onSuccess(null);
            } else {
                onSuccess(recordToCategory(result.rows.item(0)));
            }
        }, onError);
    }

    function addCategoryCount(categoryId, count, tx, onSuccess, onError) {
        tx.executeSql(sql.update.addCategoryCount, [count, categoryId], onSuccess, onError);
    }

    function addFlashcard(flashcard, tx, onSuccess, onError) {
        prepareFlashcardObject(flashcard);
        tx.executeSql(sql.insert.flashcard, [
            flashcard.categoryId,
            flashcard.front.text,
            flashcard.back.text,
            flashcard.front.image,
            flashcard.back.image,
            flashcard.stars,
            flashcard.date || new Date().getTime()
        ], function onInsertComplete(tx, result) {
            addCategoryCount(flashcard.categoryId, 1, tx, function onUpdateComplete() {
                onSuccess(result.insertId, tx);
            }, onError);
        }, onError);
    }

    function getActiveFlashcards(tx, onSuccess, onError) {
        tx.executeSql(sql.select.activeFlashcards, [], function onSelectSuccess(tx, result) {
            var i = 0, rows = result.rows, len = rows.length, record = null, flashcards = [];
            for (; i < len; i += 1) {
                record = rows.item(i);
                flashcards.push(recordToFlashcard(record));
            }
            onSuccess(flashcards);
        }, onError);
    }

    function updateFlashcardStars(id, stars, tx, onSuccess, onError) {
        tx.executeSql(sql.update.flashcardStars, [stars, new Date().getTime(), id], onSuccess, onError);
    }

    function removeFlashcard(flashcard, tx, onSuccess, onError) {
        tx.executeSql(sql.remove.flashcard, [flashcard.id], function onRemoveComplete(tx) {
            addCategoryCount(flashcard.categoryId, -1, tx, onSuccess, onError);
        }, onError);
    }

    function getFlashcard(id, tx, onSuccess, onError) {
        tx.executeSql(sql.select.flashcard, [id], function onComplete(tx, result) {
            if (result.rows.length === 0) {
                onSuccess(null);
            } else {
                onSuccess(recordToFlashcard(result.rows.item(0)));
            }
        }, onError);
    }

    function changeCategory(oldId, newId, tx, onSuccess, onError) {
        addCategoryCount(oldId, -1, tx, function onComplete() {
            addCategoryCount(newId, 1, tx, onSuccess, onError);
        }, onError);
    }

    function prepareFlashcardObject(flashcard) {
        if (!!flashcard.front.image) {
            flashcard.front.text = '';
        } else {
            flashcard.front.image = '';
        }
        if (!!flashcard.back.image) {
            flashcard.back.text = '';
        } else {
            flashcard.back.image = '';
        }
    }

    function updateFlashcard(flashcard, tx, onSuccess, onError) {

        function executeUpdate(oldCategoryId, newCategoryId) {
            prepareFlashcardObject(flashcard);
            tx.executeSql(sql.update.flashcard, [
                flashcard.categoryId,
                flashcard.front.text,
                flashcard.back.text,
                flashcard.front.image,
                flashcard.back.image,
                flashcard.id
            ], onSuccess.bind(null, {
                changedCategory: oldCategoryId !== newCategoryId,
                oldCategoryId: oldCategoryId,
                newCategoryId: newCategoryId
            }), onError);
        }

        getFlashcard(flashcard.id, tx, function onComplete(dbFlashcard) {
            var oldId = dbFlashcard.categoryId,
                newId = flashcard.categoryId;

            if (oldId !== newId) {
                changeCategory(oldId, newId, tx, executeUpdate.bind(null, oldId, newId), onError);
            } else {
                executeUpdate(oldId, oldId);
            }
        }, onError);
    }

    return {

        init: db.transaction(init),

        addCategory: db.transaction(addCategory),

        getCategories: db.transaction(getCategories),

        removeCategory: db.transaction(removeCategory),

        setCategoryName: db.transaction(setCategoryName),

        setCategoryActive: db.transaction(setCategoryActive),

        getCategoryByName: db.transaction(getCategoryByName),

        addFlashcard: db.transaction(addFlashcard),

        getActiveFlashcards: db.transaction(getActiveFlashcards),

        updateFlashcardStars: db.transaction(updateFlashcardStars),

        removeFlashcard: db.transaction(removeFlashcard),

        getFlashcard: db.transaction(getFlashcard),

        updateFlashcard: db.transaction(updateFlashcard)
    };
});
