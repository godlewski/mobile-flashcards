/* global require, document, setTimeout */

// setTimeout because without timeout splash screen is rendered slowly.
setTimeout(function afterDelay() {
    'use strict';

    require.config({
        paths: {
            'libs/angular': '../../bower_components/angular/angular.min',

            'libs/angular/animate': '../../bower_components/angular-animate/angular-animate.min',

            'libs/angular/sanitize': '../../bower_components/angular-sanitize/angular-sanitize.min',

            'libs/angular/ui-router': '../../bower_components/angular-ui-router/release/angular-ui-router.min',

            'libs/ionic': '../../bower_components/ionic/release/js/ionic.bundle.min',

            'libs/q': '../../bower_components/q/q'
        },

        shim: {
            'libs/angular': {
                exports: 'angular'
            },

            'libs/angular/animate': {
                deps: ['libs/angular']
            },

            'libs/angular/sanitize': {
                deps: ['libs/angular']
            },

            'libs/angular/ui-router': {
                deps: ['libs/angular']
            },

            'libs/ionic': {
                deps: [
                    'libs/angular',
                    'libs/angular/animate',
                    'libs/angular/sanitize',
                    'libs/angular/ui-router'
                ],
                exports: 'ionic'
            },

            'libs/q': {
                exports: 'Q'
            }
        }
    });

    require([

        // init modules
        'libs/angular', 'angular/app', 'angular/router',

        // services
        'angular/services/categories-service', 'angular/services/flashcards-service',
        'angular/services/choose-image-service', 'angular/services/services-registry',

        // directives
        'angular/directives/flashcard-view', 'angular/directives/categories-list-item',
        'angular/directives/add-category', 'angular/directives/stars', 'angular/directives/start-page',

        // controllers
        'angular/controllers/start-controller', 'angular/controllers/categories-controller',
        'angular/controllers/edit-flashcard-controller',
        'angular/controllers/images-controller'

        // tizen specific
        // , 'logic/tizen'

    ], function initApp(angular, app) {
        angular.bootstrap(document.body, [app.name]);
    });

}, 500);
