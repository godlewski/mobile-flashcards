/*global define*/

define(['angular/app'], function routerModule(app) {
    'use strict';

    app.config(['$stateProvider', '$urlRouterProvider',
        function defineRoutes($stateProvider, $urlRouterProvider) {

            $stateProvider

                .state('app', {
                    url: '/app',
                    abstract: true,
                    templateUrl: 'templates/pages/menu-wrapper.html'
                })

                .state('app.start', {
                    url: '/start',
                    views: {
                        'menuContent': {
                            templateUrl: 'templates/pages/start-page.html',
                            controller: 'StartController'
                        }
                    }
                })
                .state('app.login', {
                    url: '/login',
                    views: {
                        'menuContent': {
                            templateUrl: 'templates/pages/login-page.html'
                        }
                    }
                })
                .state('app.categories', {
                    url: '/categories',
                    views: {
                        'menuContent': {
                            templateUrl: 'templates/pages/categories-page.html',
                            controller: 'CategoriesController'
                        }
                    }
                })
                .state('app.flashcard-form', {
                    url: '/flashcard-form/{action}/{id}',
                    views: {
                        'menuContent': {
                            templateUrl: 'templates/pages/edit-flashcard-page.html',
                            controller: 'FlashcardController'
                        }
                    }
                })
                .state('app.images', {
                    url: '/images',
                    views: {
                        'menuContent': {
                            templateUrl: 'templates/pages/images-page.html',
                            controller: 'ImagesController'
                        }
                    }
                })
                .state('app.about', {
                    url: '/about',
                    views: {
                        'menuContent': {
                            templateUrl: 'templates/pages/about.html'
                        }
                    }
                });

            $urlRouterProvider.otherwise('/app/start');

        }]);
});
