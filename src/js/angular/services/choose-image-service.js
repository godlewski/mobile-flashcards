/*global define*/

define([
    'angular/app'
], function chooseImageServiceModule(app) {
    'use strict';

    app.factory('ChooseImageService', [function chooseImageService() {

        var imageListener = null;

        function setChooseImageListener(listener) {
            imageListener = listener;
        }

        function setImage(image) {
            if (typeof imageListener === 'function') {
                imageListener(image);
            }
        }

        return {
            setChooseImageListener: setChooseImageListener,
            setImage: setImage
        };

    }]);
});
