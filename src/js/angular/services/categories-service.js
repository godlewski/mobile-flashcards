/*global define*/

define(['angular/app', 'logic/storage/db'], function categoriesServiceModule(app, db) {
    'use strict';

    app.factory('CategoriesService', ['$rootScope', 'ServicesRegistry', function service($rootScope, ServicesRegistry) {

        var categories = [],
            me = null,
            FlashcardsService = null;

        function addCategory(category) {
            return db.addCategory(category)
                .then(function onSuccess(id) {
                    category.id = id;
                    categories.push(category);
                    $rootScope.$apply();
                });
        }

        function getCategories() {
            return categories;
        }

        function getCategory(id) {
            var i = 0, len = categories.length;
            for (; i < len; i += 1) {
                if (categories[i].id === id) {
                    return categories[i];
                }
            }
        }

        function removeLocalCategory(id) {
            var i = 0, len = categories.length;
            for (; i < len; i += 1) {
                if (categories[i].id === id) {
                    categories.splice(i, 1);
                    return;
                }
            }
        }

        function removeCategory(id) {
            var wasEmpty = isCategoryEmpty(id);
            return db.removeCategory(id).then(function onRemoved() {
                removeLocalCategory(id);
                $rootScope.$apply();
            }).then(function onComplete() {
                if (!wasEmpty) {
                    return FlashcardsService.reloadFromDB();
                }
            });
        }

        function setCategoryName(id, name) {
            return db.setCategoryName(id, name);
        }

        function isCategoryEmpty(id) {
            var i = 0, len = categories.length;
            for (; i < len; i += 1) {
                if (categories[i].id === id && categories[i].count > 0) {
                    return false;
                }
            }
            return true;
        }

        function setCategoryActive(id, active) {
            return db.setCategoryActive(id, active)
                .then(function onComplete() {
                    if (!isCategoryEmpty(id)) {
                        return FlashcardsService.reloadFromDB();
                    }
                });
        }

        function getCategoryByName(name) {
            return db.getCategoryByName(name);
        }

        function increaseCategoryCount(categoryId) {
            var i, len, category;

            for (i = 0, len = categories.length; i < len; i += 1) {
                category = categories[i];
                if (category.id === categoryId) {
                    category.count += 1;
                    return;
                }
            }
        }

        function decreaseCategoryCount(categoryId) {
            var i, len, category;

            for (i = 0, len = categories.length; i < len; i += 1) {
                category = categories[i];
                if (category.id === categoryId) {
                    category.count -= 1;
                    return;
                }
            }
        }

        function isCategoryActive(id) {
            var i = 0, len = categories.length, category;
            for (; i < len; i += 1) {
                category = categories[i];
                if (category.id === id && category.active) {
                    return true;
                }
            }
            return false;
        }

        function init() {

            return ServicesRegistry.getService('FlashcardsService').then(function onComplete(service) {
                FlashcardsService = service;
                return db.init();
            }).then(function onInitialized() {
                return db.getCategories();
            }).then(function onGet(result) {
                var i = 0,
                    len = result.length;

                for (; i < len; i += 1) {
                    categories.push(result[i]);
                }
            });
        }

        me = {
            init: init,
            addCategory: addCategory,
            setCategoryName: setCategoryName,
            setCategoryActive: setCategoryActive,
            removeCategory: removeCategory,
            getCategoryByName: getCategoryByName,
            getCategories: getCategories,
            getCategory: getCategory,
            increaseCategoryCount: increaseCategoryCount,
            decreaseCategoryCount: decreaseCategoryCount,
            isCategoryActive: isCategoryActive
        };

        ServicesRegistry.registerService('CategoriesService', me);

        return me;
    }]);
});
