/*global define*/

define([
    'angular/app',
    'logic/storage/db',
    'logic/random-generator'
], function categoriesServiceModule(app, db, RandomGenerator) {
    'use strict';

    app.factory('FlashcardsService', ['ServicesRegistry', function flashcardsService(ServicesRegistry) {
        var generator = null,
            me = null,
            CategoriesService = null;

        function nextFlashcard() {
            return generator.next();
        }

        function addFlashcard(flashcard) {
            return db.addFlashcard(flashcard).then(function onAdded(id) {
                flashcard.id = id;
                return CategoriesService.increaseCategoryCount(flashcard.categoryId);
            }).then(function onComplete() {
                if (CategoriesService.isCategoryActive(flashcard.categoryId)) {
                    generator.addFlashcard(flashcard);
                }
            });
        }

        function updateFlashcardStars(flashcard) {
            flashcard.lastModified = new Date().getTime();
            return db.updateFlashcardStars(flashcard.id, flashcard.stars).then(function onUpdatedDb() {
                generator.updateFlashcard(flashcard);
            });
        }

        function removeFlashcard(flashcard) {
            if (CategoriesService.isCategoryActive(flashcard.categoryId)) {
                generator.removeFlashcard(flashcard);
            }
            return db.removeFlashcard(flashcard).then(function onRemoved() {
                return CategoriesService.decreaseCategoryCount(flashcard.categoryId);
            });
        }

        function getFlashcard(id) {
            return db.getFlashcard(id);
        }

        function updateFlashcard(flashcard) {
            return db.updateFlashcard(flashcard).then(function onComplete(result) {
                if (result.changedCategory) {
                    CategoriesService.increaseCategoryCount(result.newCategoryId);
                    CategoriesService.decreaseCategoryCount(result.oldCategoryId);
                }
                generator.updateFlashcard(flashcard);
            });
        }

        function reloadFromDB() {
            return db.getActiveFlashcards().then(function onGetFlashcards(flashcards) {
                generator = new RandomGenerator(flashcards);
            });
        }

        function init() {
            return ServicesRegistry.getService('CategoriesService').then(function onComplete(service) {
                CategoriesService = service;
                return reloadFromDB();
            });
        }

        me = {
            init: init,
            reloadFromDB: reloadFromDB,
            nextFlashcard: nextFlashcard,
            addFlashcard: addFlashcard,
            updateFlashcardStars: updateFlashcardStars,
            removeFlashcard: removeFlashcard,
            getFlashcard: getFlashcard,
            updateFlashcard: updateFlashcard
        };

        ServicesRegistry.registerService('FlashcardsService', me);

        return me;
    }]);
});
