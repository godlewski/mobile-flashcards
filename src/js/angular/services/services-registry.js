/*global define*/

define(['angular/app'], function eventServiceModule(app) {
    'use strict';

    app.factory('ServicesRegistry', ['$q', function servicesRegistry($q) {
        var services = {},
            promises = {};

        function registerService(name, service) {
            services[name] = service;
            if (promises[name]) {
                promises[name](service);
            }
        }

        function getService(name) {
            return $q(function promise(resolve) {
                var service = services[name];
                if (service) {
                    resolve(service);
                } else {
                    promises[name] = resolve;
                }
            });
        }

        return {
            registerService: registerService,
            getService: getService
        };
    }]);
});
