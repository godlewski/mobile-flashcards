/*global define, screen, window, setTimeout*/
define(['angular/app', 'logic/helpers/rotation'], function flashcardView(app, rotation) {
    'use strict';

    var previousScreenPoint,

        screenWidth,
        flashcardEl,

        SPEED_RATIO = 1;

    function onTouchStart(e) {
        rotation.start();
        if (rotation.isFront()) {
            flashcardEl.classList.remove('reset-front');
            flashcardEl.classList.remove('reset-front-360');
        } else {
            flashcardEl.classList.remove('reset-back');
        }
        flashcardEl.style.webkitTransform = 'perspective(1000) rotateY(' + rotation.getAngle() + 'deg)';
        previousScreenPoint = e.changedTouches[0].screenX;
        e.stopPropagation();
    }

    function onTouchMove(e) {
        var screenPoint = e.changedTouches[0].screenX,
            diff = SPEED_RATIO * 180 * (screenPoint - previousScreenPoint) / screenWidth,
            angle = rotation.rotate(diff);

        previousScreenPoint = screenPoint;
        flashcardEl.style.webkitTransform = 'perspective(1000) rotateY(' + angle + 'deg)';
        e.stopPropagation();
    }

    function onTouchEnd(e) {
        if (rotation.isFront()) {
            if (rotation.getAngle() > 270) {
                flashcardEl.classList.add('reset-front-360');
            } else {
                flashcardEl.classList.add('reset-front');
            }
        } else {
            flashcardEl.classList.add('reset-back');
        }
        e.stopPropagation();
    }

    app.directive('flashcard', ['$q', function directive($q) {

        function initController(controller, element) {
            var state = '',
                hideCallback = null,
                frontSide = element.querySelector('.side.front'),
                backSide = element.querySelector('.side.back'),
                frontText = element.querySelector('.side.front h1'),
                backText = element.querySelector('.side.back h1'),
                frontImage = frontSide.querySelector('img'),
                backImage = backSide.querySelector('img'),
                maxTextHeight = parseInt(window.getComputedStyle(frontText, null).maxHeight);

            element.addEventListener('webkitTransitionEnd', function onTransitionEnd() {
                if (state === 'hiding') {
                    element.classList.add('non-transition');
                    element.classList.add('hide-up');
                    element.classList.remove('hide-down');
                    state = 'hidden';
                    hideCallback();
                } else {
                    if (frontText.clientHeight === maxTextHeight) {
                        frontText.style.overflowY = 'scroll';
                    } else {
                        frontText.style.overflowY = 'hidden';
                    }
                    if (backText.clientHeight === maxTextHeight) {
                        backText.style.overflowY = 'scroll';
                    } else {
                        backText.style.overflowY = 'hidden';
                    }
                }
            });
            controller.hide = function hide() {
                return $q(function promise(resolve) {
                    state = 'hiding';
                    hideCallback = resolve;
                    element.classList.add('hide-down');
                });
            };

            controller.show = function show() {
                $q(function promise(resolve) {
                    state = '';
                    element.classList.remove('non-transition');
                    element.classList.remove('hide-up');
                    var objCassList = element.querySelector('.flashcard-object').classList;
                    if (!objCassList.contains('reset-front')) {
                        objCassList.add('non-transition');
                        objCassList.remove('reset-back');
                        objCassList.remove('reset-front-360');
                        objCassList.add('reset-front');
                        rotation.restart();

                        setTimeout(function test() {
                            objCassList.remove('non-transition');
                            resolve();
                        }, 500);
                    } else {
                        resolve();
                    }
                });
            };

            controller.showFrontImage = function showFrontImage(base64Image) {
                frontImage.src = base64Image;
                if (!frontSide.classList.contains('with-image')) {
                    frontSide.classList.add('with-image');
                }
            };

            controller.hideFrontImage = function hideFrontImage() {
                frontSide.classList.remove('with-image');
            };

            controller.showBackImage = function showBackImage(base64Image) {
                backImage.src = base64Image;
                if (!backSide.classList.contains('with-image')) {
                    backSide.classList.add('with-image');
                }
            };

            controller.hideBackImage = function hideBackImage() {
                backSide.classList.remove('with-image');
            };
        }

        function onLinkView(scope, angularElement) {
            var flashcardTouchArea = angularElement[0];

            flashcardEl = flashcardTouchArea.querySelector('.flashcard-object');
            screenWidth = screen.width;

            flashcardTouchArea.addEventListener('touchstart', onTouchStart, false);
            flashcardTouchArea.addEventListener('touchmove', onTouchMove, false);
            flashcardTouchArea.addEventListener('touchend', onTouchEnd, false);

            initController(scope.controller, flashcardTouchArea);
        }

        return {
            restrict: 'A',
            templateUrl: 'templates/directives/flashcard-view.html',
            scope: {
                flashcard: '=flashcard',
                category: '=category',
                controller: '=controller'
            },
            link: onLinkView
        };
    }]);
});
