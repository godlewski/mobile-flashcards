/*global define*/
define(['angular/app'], function categoryListItem(app) {
    'use strict';

    function link(scope, element) {

        function onToggle() {
            scope.onToggle({category: scope.category});
        }

        var toggle = element[0].querySelector('input[type="checkbox"]');
        toggle.addEventListener('change', onToggle);
        scope.$on('$destroy', function onDestroy() {
            toggle.removeEventListener('change', onToggle);
        });
    }

    app.directive('categoriesListItem', [function directive() {
        return {
            restrict: 'A',
            templateUrl: 'templates/directives/category-list-item.html',
            scope: {
                'onToggle': '&',
                'category': '='
            },
            link: link
        };
    }]);
});
