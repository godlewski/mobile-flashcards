/*global define, screen*/
define(['angular/app'], function starsView(app) {
    'use strict';

    app.directive('starsView', [function directive() {

        function onLinkView(scope, element) {
            scope.elements = element[0].getElementsByTagName('i');
            for (var i = 0; i < 5; i += 1) {
                scope.elements[i].classList.add(
                    scope.stars > i ? 'ion-android-star' : 'ion-android-star-outline'
                );
            }
            scope.$watch('stars', function (value) {
                var stars = parseInt(value, 10);
                for (var i = 0; i < 5; i += 1) {
                    scope.elements[i].classList.add(
                        stars > i ? 'ion-android-star' : 'ion-android-star-outline'
                    );
                    scope.elements[i].classList.remove(
                        stars > i ? 'ion-android-star-outline' : 'ion-android-star'
                    );
                }
            });
        }

        return {
            restrict: 'A',
            templateUrl: 'templates/directives/stars-view.html',
            scope: {
                stars: '@'
            },
            link: onLinkView
        };
    }]);
});
