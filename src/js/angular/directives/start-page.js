/*global define, screen, setTimeout, document*/
define(['angular/app'], function flashcardView(app) {
    'use strict';

    app.directive('startPage', [function directive() {

        return {
            restrict: 'A',
            scope: {
                controller: '=controller'
            },
            link: function onLink(scope) {
                scope.controller.ready = function ready() {
                    // setTimeout to ensure that page is rendered before removing the splash.
                    setTimeout(function onReady() {
                        document.body.removeChild(document.querySelector('.splash'));
                    }, 400);
                };
            }
        };
    }]);
});
