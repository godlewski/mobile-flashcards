/*global define, document*/
define(['angular/app'], function flashcardView(app) {
    'use strict';

    app.directive('addCategory', ['$ionicPopup', function directive($ionicPopup) {
        function onClick(scope) {
            if (!scope.onEnterName) {
                return;
            }
            scope.$ionicPopup.show({
                template: '<input type="text" placeholder="Category Name" id="category-name-input">',
                title: 'New Category',
                buttons: [{
                    text: 'Cancel',
                    type: 'button-default'
                }, {
                    text: 'OK',
                    type: 'button-positive',
                    onTap: function onTap() {
                        var name = document.getElementById('category-name-input').value;
                        scope.onEnterName({name: name});
                    }
                }]
            });
        }

        function onDestroy(scope) {
            scope.el.removeEventListener('click', scope.onClickHandler);
        }

        function onLinkView(scope, angularElement) {
            scope.el = angularElement[0];
            scope.$ionicPopup = $ionicPopup;
            scope.onClickHandler = onClick.bind(null, scope);
            scope.el.addEventListener('click', scope.onClickHandler);
            scope.$on('$destroy', onDestroy.bind(null, scope));
        }

        return {
            restrict: 'A',
            scope: {
                onEnterName: '&onEnterName'
            },
            link: onLinkView
        };
    }]);
});
