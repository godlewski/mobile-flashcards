/*global define, document, setTimeout*/
define([
    'angular/app',
    'libs/angular',
    'logic/device/image-picker',
    'libs/ionic',
], function flashcardControllerModule(app, angular, imagePicker, ionic) {
    'use strict';

    app.controller('FlashcardController', [
        'CategoriesService',
        'FlashcardsService',
        'ChooseImageService',
        '$ionicPopup',
        '$state',
        '$stateParams',
        '$scope',
        '$rootScope',
        /* jshint maxparams: 8 */
        function flashcardController(CategoriesService, FlashcardsService, ChooseImageService, $ionicPopup, $state,
                                     $stateParams, $scope, $rootScope) {


            function createFlashcard() {
                return {
                    stars: 0,
                    categoryId: 0,
                    front: {
                        text: ''
                    },
                    back: {
                        text: ''
                    }
                };
            }

            function savePage() {
                ionic.DomUtil.blurAll();
                $scope.flashcard[$scope.currentSide].text = $scope.text;
            }

            function clearPage() {
                $scope.flashcard = createFlashcard();
                $scope.currentSide = 'front';
                $scope.text = '';
                $scope.category = '';
            }

            $scope.backFromImageChooser = false;

            $scope.getCategories = function getCategories() {
                return CategoriesService.getCategories();
            };

            $scope.setCurrentSide = function setCurrentSide(side) {
                if ($scope.currentSide !== side) {
                    savePage();
                    $scope.currentSide = side;
                    $scope.text = $scope.flashcard[$scope.currentSide].text;
                }
            };

            $scope.getText = function getText() {
                return $scope.flashcard[$scope.currentSide].text;
            };

            $scope.getImage = function getImage() {
                return $scope.flashcard[$scope.currentSide].image;
            };

            $scope.setImage = function setImage(base64image) {
                $scope.flashcard[$scope.currentSide].image = base64image;
            };

            $scope.rotateImage = function rotateImage() {
                imagePicker
                    .rotate($scope.flashcard[$scope.currentSide].image)
                    .then(function onRotated(base64Image) {
                        $scope.flashcard[$scope.currentSide].image = base64Image;
                        $rootScope.$apply();

                        // Start Tizen workaround...
                        var x = document.querySelector('.image-container');
                        x.style.maxWidth = '90%';
                        setTimeout(function afterTimeout() {
                            x.style.maxWidth = '100%';
                        }, 50);
                        // Finish workaround
                    });
            };

            $scope.clearImage = function clearImage() {
                $scope.setImage(null);
            };

            $scope.isFrontSide = function isFrontSide() {
                return $scope.currentSide === 'front';
            };

            $scope.isImageLoaded = function isImageLoaded() {
                return !!$scope.getImage();
            };

            function showValidateAlert() {
                $ionicPopup.alert({
                    title: 'Warning',
                    template: 'Please fill all fields'
                });
            }

            function validate() {
                return !!$scope.category &&
                    ($scope.flashcard.front.text.trim() !== '' || !!$scope.flashcard.front.image) &&
                    ($scope.flashcard.back.text.trim() !== '' || !!$scope.flashcard.back.image);
            }

            $scope.save = function save() {
                savePage();

                if (!validate()) {
                    showValidateAlert();
                    return;
                }

                CategoriesService.getCategoryByName($scope.category)
                    .then(function onGetCategory(category) {
                        $scope.flashcard.categoryId = category.id;
                    })
                    .then(function onSuccess() {
                        if ($scope.flashcard.id) {
                            return FlashcardsService.updateFlashcard($scope.flashcard);
                        } else {
                            return FlashcardsService.addFlashcard($scope.flashcard);
                        }
                    })
                    .then(function onSuccess() {
                        $state.go('app.start');
                    });
            };

            function onPageShow(params) {
                if ($scope.backFromImageChooser) {
                    $scope.backFromImageChooser = false;
                    return;
                }
                if (params.action === 'add') {
                    clearPage();
                } else {
                    FlashcardsService.getFlashcard(Number(params.id)).then(function onComplete(flashcard) {
                        $scope.flashcard = angular.copy(flashcard);
                        $scope.currentSide = 'front';
                        $scope.text = $scope.flashcard.front.text;
                        $scope.category = CategoriesService.getCategory($scope.flashcard.categoryId).name;
                    });
                }
            }

            function init() {
                clearPage();

                $rootScope.$on('$stateChangeStart',
                    function onPageChanged(event, toState, toParams) {
                        if (toState.name === 'app.flashcard-form') {
                            onPageShow(toParams);
                        }
                    });

                ChooseImageService.setChooseImageListener(function onChosenImage(base64Image) {
                    $scope.backFromImageChooser = true;
                    $scope.setImage(base64Image);
                    $rootScope.$apply();
                });

                onPageShow($stateParams);
            }

            init();
        }
    ]);
});
