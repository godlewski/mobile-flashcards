/*global define, document*/
define(['angular/app'], function categoriesControllerModule(app) {
    'use strict';

    app.controller('CategoriesController', [
        'CategoriesService',
        '$ionicPopup',
        '$scope',
        function categoriesController(CategoriesService, $ionicPopup, $scope) {

            $scope.searchBox = '';
            $scope.editMode = false;
            $scope.categories = CategoriesService.getCategories();

            $scope.addCategory = function addCategory(name) {
                if (!name || name.length === 0) {
                    $ionicPopup.alert({
                        title: 'Warning',
                        template: 'Please enter non-empty name'
                    });
                    return;
                }

                CategoriesService.addCategory({
                    name: name,
                    count: 0,
                    active: true
                }).catch(function onError() {
                    $ionicPopup.alert({
                        title: 'Warning',
                        template: 'Such category already exists'
                    });
                });
            };

            /**
             * Ionic bug workaround.
             */
            function removePopupTrash() {
                var popupTrash = document.querySelector('.popup-container');
                if (popupTrash) {
                    popupTrash.parentNode.removeChild(popupTrash);
                }
            }

            function removeCategory(id) {
                CategoriesService.removeCategory(id);
            }

            function renameCategory(category, newName) {
                if (newName.length === 0) {
                    $ionicPopup.alert({
                        title: 'Warning',
                        template: 'Please enter non-empty name'
                    });
                    return;
                }
                category.name = newName;
                CategoriesService.setCategoryName(category.id, category.name)
                    .catch(function onError() {
                        $ionicPopup.alert({
                            title: 'Warning',
                            template: 'Such category already exists'
                        });
                    });
            }

            function showRenameCategoryPopup(category) {
                $ionicPopup.prompt({
                    title: 'Put the new name',
                    inputType: 'text',
                    inputPlaceholder: 'Category Name'
                }).then(function onTap(newName) {
                    renameCategory(category, newName);
                });
            }

            function showRemoveCategoryPopup(category) {
                $ionicPopup.confirm({
                    title: 'Remove ' + category.name,
                    template: 'Are you sure you want to remove the "' + category.name +
                    '" category with all it\'s flashcards?',
                    okText: 'Yes',
                    cancelText: 'No'
                }).then(function onAnswered() {
                    removeCategory(category.id);
                });
            }


            $scope.onHoldCategory = function onHoldCategory(category) {
                $ionicPopup.show({
                    cssClass: 'category-popup-menu',
                    buttons: [{
                        text: '<i class="ion-edit"> <span class="action-title">Rename</span>',
                        onTap: function onTap() {
                            removePopupTrash();
                            showRenameCategoryPopup(category);
                        }
                    }, {
                        text: '<i class="ion-trash-a"> <span class="action-title">Remove</span>',
                        onTap: function onTap() {
                            removePopupTrash();
                            showRemoveCategoryPopup(category);
                        }
                    }, {text: '<i class="ion-ios-arrow-back"> <span class="action-title">Cancel</span>'}]
                });
            };

            $scope.onToggleCategory = function onToggleCategory(category) {
                CategoriesService.setCategoryActive(category.id, !category.active);
            };

        }
    ]);
});
