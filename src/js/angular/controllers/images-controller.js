/*global define, document*/
define(['angular/app', 'logic/device/image-picker'], function imagesControllerModule(app, imagePicker) {
    'use strict';

    app.controller('ImagesController', [
        '$scope',
        '$ionicHistory',
        'ChooseImageService',
        function imagesController($scope, $ionicHistory, ChooseImageService) {

            $scope.searchBox = '';
            $scope.images = [];

            $scope.chooseImage = function chooseImage(path) {
                imagePicker.imageAsBase64(path).then(function (data) {
                    ChooseImageService.setImage(data);
                    $ionicHistory.goBack();
                });
            };

            imagePicker.getImages().then(function onComplete(images) {
                var i = 0, len = images.length;

                for (; i < len; i += 1) {
                    $scope.images.push(images[i]);
                }
            });

        }
    ]);
});
