/*global define*/
define(['angular/app'], function startController(app) {
    'use strict';

    app.controller('StartController', [
        'CategoriesService',
        'FlashcardsService',
        '$ionicPopover',
        '$ionicPopup',
        '$scope',
        '$rootScope',
        '$state',
        /* jshint maxparams: 7 */
        function startController(CategoriesService, FlashcardsService, $ionicPopover, $ionicPopup, $scope, $rootScope,
                                 $state) {

            var EMPTY_FLASHCARD = {
                    front: {
                        text: 'No flashcards'
                    },
                    back: {
                        text: 'Create some'
                    }
                },
                EMPTY_CATEGORY_NAME = 'Hello',

                disabledButtons = false;


            $scope.flashcard = EMPTY_FLASHCARD;
            $scope.categoryName = EMPTY_CATEGORY_NAME;

            function editFlashcard() {
                $state.go('app.flashcard-form', {action: 'edit', id: $scope.flashcard.id});
            }

            function removeFlashcard() {
                $ionicPopup.confirm({
                    title: 'The flashcard will be removed'
                }).then(function onTapButton(res) {
                    if (res) {
                        FlashcardsService.removeFlashcard($scope.flashcard).then(loadNextFlashcard)
                            .then(function onComplete() {
                                $scope.$apply();
                            });
                    }
                });
            }

            $scope.flashcardController = {};
            $scope.pageController = {};

            $scope.starUp = function starUp() {
                if (disabledButtons) {
                    return;
                }
                disabledButtons = true;
                $scope.flashcardController.hide().then(function onHidden() {
                    var flashcard = $scope.flashcard;
                    if (!$scope.noFlashcards && flashcard.stars < 5) {
                        flashcard.stars += 1;
                        return FlashcardsService.updateFlashcardStars(flashcard);
                    }
                }).then(loadNextFlashcard).then(function onChangedFlashcard() {
                    return $scope.flashcardController.show();
                }).then(function onComplete() {
                    disabledButtons = false;
                });
            };


            $scope.starDown = function starUp() {
                if (disabledButtons) {
                    return;
                }
                disabledButtons = true;
                $scope.flashcardController.hide().then(function onHidden() {
                    var flashcard = $scope.flashcard;
                    if (!$scope.noFlashcards && flashcard.stars > 0) {
                        flashcard.stars -= 1;
                        return FlashcardsService.updateFlashcardStars(flashcard);
                    }
                }).then(loadNextFlashcard).then(function onChangedFlashcard() {
                    $scope.flashcardController.show();
                }).then(function onComplete() {
                    disabledButtons = false;
                });
            };

            $ionicPopover.fromTemplateUrl('templates/popovers/flashcard-menu.html', {
                scope: $scope
            }).then(function onCreate(popover) {

                $scope.showPopover = function showPopover($event) {
                    popover.show($event);
                };

                $scope.$on('$destroy', function onDestroy() {
                    popover.remove();
                });

                $scope.editFlashcard = function onEditFlashcard() {
                    popover.hide();

                    if (!$scope.noFlashcards) {
                        editFlashcard();
                    }
                };

                $scope.removeFlashcard = function onRemoveFlashcard() {
                    popover.hide();
                    if (!$scope.noFlashcards) {
                        removeFlashcard();
                    }
                };
            });

            function loadNextFlashcard() {
                var flashcard = FlashcardsService.nextFlashcard();
                if (!!flashcard) {
                    $scope.flashcard = flashcard;
                    $scope.categoryName = CategoriesService.getCategory(flashcard.categoryId).name;
                    $scope.noFlashcards = false;
                    if (!!flashcard.front.image) {
                        $scope.flashcardController.showFrontImage(flashcard.front.image);
                    } else {
                        $scope.flashcardController.hideFrontImage();
                    }
                    if (!!flashcard.back.image) {
                        $scope.flashcardController.showBackImage(flashcard.back.image);
                    } else {
                        $scope.flashcardController.hideBackImage();
                    }
                } else {
                    $scope.flashcard = EMPTY_FLASHCARD;
                    $scope.categoryName = EMPTY_CATEGORY_NAME;
                    $scope.noFlashcards = true;
                    $scope.flashcardController.hideFrontImage();
                    $scope.flashcardController.hideBackImage();
                }
            }

            function init() {
                CategoriesService.init().then(function onInitialized() {
                    return FlashcardsService.init();
                }).then(loadNextFlashcard).then(function onLoaded() {
                    $scope.pageController.ready();
                });

                $rootScope.$on('$stateChangeStart',
                    function onPageChanged(event, toState) {
                        if (toState.name === 'app.start') {
                            loadNextFlashcard();
                        }
                    });
            }

            init();

        }
    ]);
});
