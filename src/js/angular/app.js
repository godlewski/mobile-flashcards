/*global define*/

define(['libs/angular', 'libs/ionic'], function appModule(angular) {
    'use strict';

    return angular.module('App', [
        'ngAnimate',
        'ngSanitize',
        'ui.router',
        'ionic'
    ]);

});
