/*global module:false*/
module.exports = function executeGrunt(grunt) {
    'use strict';

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
        '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
        '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
        '* Copyright (c) <%= grunt.template.today("yyyy") %> ' +
        '<%= pkg.author.name %>;' +
        ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',

        less: {
            tizen: {
                src: ['src/less/style-tizen.less'],
                dest: 'tizen/css/style.css'
            },
            phonegap: {
                src: ['src/less/style.less'],
                dest: 'phonegap/www/css/style.css'
            }
        },
        copy: {
            tizen: {
                files: [{
                    expand: true,
                    src: [
                        'angular/angular.min.js',
                        'angular-animate/angular-animate.min.js',
                        'angular-sanitize/angular-sanitize.min.js',
                        'angular-ui-router/release/angular-ui-router.min.js',
                        'ionic/release/**',
                        'requirejs/require.js',
                        'q/q.js'
                    ],
                    dest: 'tizen/libs/',
                    cwd: 'bower_components/'
                }, {
                    expand: true,
                    src: [
                        '**/*.js',
                        '!require-init.js'
                    ],
                    dest: 'tizen/js/',
                    cwd: 'src/js/'
                }, {
                    expand: true,
                    src: ['**'],
                    dest: 'tizen/templates/',
                    cwd: 'src/templates/'
                }, {
                    expand: true,
                    src: ['**'],
                    dest: 'tizen/img/',
                    cwd: 'src/img/'
                }]

            },
            phonegap: {
                files: [{
                    expand: true,
                    src: [
                        'angular/angular.min.js',
                        'angular-animate/angular-animate.min.js',
                        'angular-sanitize/angular-sanitize.min.js',
                        'angular-ui-router/release/angular-ui-router.min.js',
                        'ionic/release/**',
                        'requirejs/require.js',
                        'q/q.js'
                    ],
                    dest: 'phonegap/www/libs/',
                    cwd: 'bower_components/'
                }, {
                    expand: true,
                    src: [
                        '**/*.js',
                        '!require-init.js'
                    ],
                    dest: 'phonegap/www/js/',
                    cwd: 'src/js/'
                }, {
                    expand: true,
                    src: ['**'],
                    dest: 'phonegap/www/templates/',
                    cwd: 'src/templates/'
                }, {
                    expand: true,
                    src: ['**'],
                    dest: 'phonegap/www/img/',
                    cwd: 'src/img/'
                }]

            }
        },

        replace: {
            tizenLess: {
                src: ['src/less/style.less'],
                dest: ['src/less/style-tizen.less'],
                replacements: [{
                    from: '// custom platforms here...',
                    to: '@import "tizen-custom";'
                }]
            },
            tizenHTML: {
                src: ['src/index.html'],
                dest: 'tizen/index.html',
                replacements: [{
                    from: '../bower_components/',
                    to: 'libs/'
                }]
            },
            tizenJS: {
                src: ['src/js/require-init.js'],
                dest: 'tizen/js/require-init.js',
                replacements: [{
                    from: '../bower_components',
                    to: 'libs'
                }, {
                    from: '// , \'logic/tizen\'',
                    to: ', \'logic/tizen\''
                }]
            },
            tizenJS2: {
                src: ['src/js/angular/controllers/images-controller.js'],
                dest: 'tizen/js/angular/controllers/images-controller.js',
                replacements: [{
                    from: 'logic/device/image-picker',
                    to: 'logic/device/tizen-image-picker'
                }]
            },
            tizenJS3: {
                src: ['src/js/angular/controllers/edit-flashcard-controller.js'],
                dest: 'tizen/js/angular/controllers/edit-flashcard-controller.js',
                replacements: [{
                    from: 'logic/device/image-picker',
                    to: 'logic/device/tizen-image-picker'
                }]
            },
            phonegapHTML: {
                src: ['src/index.html'],
                dest: 'phonegap/www/index.html',
                replacements: [{
                    from: '../bower_components/',
                    to: 'libs/'
                }]
            },
            phonegapJS: {
                src: ['src/js/require-init.js'],
                dest: 'phonegap/www/js/require-init.js',
                replacements: [{
                    from: '../bower_components',
                    to: 'libs'
                }]
            }
        },

        exec: {
            phonegapBuild: 'cd phonegap && phonegap build && cd ..'
        }

    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-text-replace');
    grunt.loadNpmTasks('grunt-exec');

    grunt.registerTask('tizen', [
        'replace:tizenLess',
        'less:tizen',
        'copy:tizen',
        'replace:tizenHTML',
        'replace:tizenJS',
        'replace:tizenJS2',
        'replace:tizenJS3'
    ]);

    grunt.registerTask('android', [
        'less:phonegap',
        'copy:phonegap',
        'replace:phonegapHTML',
        'replace:phonegapJS',
        'exec:phonegapBuild'
    ]);

};
